document.addEventListener("DOMContentLoaded", event => {
    autosize(document.querySelectorAll('textarea'));

    let uuid = document.getElementById('user_uuid').value;
    let messageContent = document.getElementById('chat-messages');

    let conn = new WebSocket(`ws://127.0.0.1:8080?uuid=${uuid}`);

    conn.onopen = function(e) {
        console.log("Connection established!");
    };

    conn.onmessage = function(e) {
        messageContent.innerHTML += messageBox(e.data, false);
    };

    document.getElementById('send').addEventListener("click", event => {
        let textarea = document.getElementById('textarea');

        messageContent.innerHTML += messageBox(textarea.value, true);

        conn.send(JSON.stringify({
            for: event.target.value,
            message: textarea.value
        }));

        textarea.value = '';
    })
})

function messageBox(message, isSender) {
    return `<div class="message-parent ${ isSender ? 'sender' : 'receiver' }">
                <div class="avatar"></div>
                <div class="message">
                    ${message}
                </div>
            </div>`;
}