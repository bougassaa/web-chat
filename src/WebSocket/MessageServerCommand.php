<?php

namespace App\WebSocket;

use Ratchet\Http\HttpServer;
use Ratchet\Server\IoServer;
use Ratchet\WebSocket\WsServer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MessageServerCommand extends Command
{

    private MessageSocket $socket;

    public function __construct(MessageSocket $socket)
    {
        $this->socket = $socket;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('message-server')
            ->setAliases(['ms'])
            ->setDescription('Run WebSocket for real time message');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $server = IoServer::factory(
            new HttpServer(
                new WsServer(
                    $this->socket
                )
            ),
            8080
        );

        $output->writeln('WebSocket server launched...');
        $server->run();
    }
}