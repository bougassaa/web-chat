<?php

namespace App\WebSocket;

use App\Entity\Message;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\Persistence\ManagerRegistry;
use Psr\Log\LoggerInterface;
use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;

class MessageSocket implements MessageComponentInterface
{

    private \SplObjectStorage $clients;
    private LoggerInterface $logger;
    private UserRepository $userRepo;
    private ManagerRegistry $doctrine;

    public function __construct(LoggerInterface $logger, UserRepository $userRepo, ManagerRegistry $doctrine)
    {
        $this->clients = new \SplObjectStorage();
        $this->logger = $logger;
        $this->userRepo = $userRepo;
        $this->doctrine = $doctrine;
    }

    function onOpen(ConnectionInterface $conn)
    {
        parse_str($conn->httpRequest->getUri()->getQuery());

        if (!isset($uuid)){
            $conn->close();
            return;
        }

        $this->clients->attach($conn, $uuid);
    }

    function onClose(ConnectionInterface $conn)
    {
        $this->clients->detach($conn);
    }

    function onError(ConnectionInterface $conn, \Exception $e)
    {
        $this->logger->error("Socket error : " . $e->getMessage());
    }

    function onMessage(ConnectionInterface $from, $data)
    {
        $data = json_decode($data);

        $message = new Message();
        $message->setContent($data->message)
            ->setSender($this->getUserFromConnection($from))
            ->setReceiver($this->userRepo->getUserFromUuid($data->for))
            ->setSendAt(new \DateTimeImmutable());

        $manager = $this->doctrine->getManager();
        $manager->persist($message);
        $manager->flush();

        foreach ($this->clients as $client) {
            if ($this->clients->getInfo() == $data->for) {
                $client->send($data->message);
            }
        }
    }

    private function getUserFromConnection(ConnectionInterface $connection): ?User
    {
        foreach ($this->clients as $client) {
            if ($client == $connection) {
                return $this->userRepo->getUserFromUuid(
                    $this->clients->getInfo()
                );
            }
        }

        return null;
    }
}