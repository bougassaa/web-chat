<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ChatController extends AbstractController
{
    /**
     * @Route("/chat/{for}", name="chat")
     */
    public function index(UserRepository $repository, string $for = ''): Response
    {
        return $this->render('chat/index.html.twig', [
            'for' => $for,
            'users' => $repository->getUsersExceptOne($this->getUser())
        ]);
    }
}
